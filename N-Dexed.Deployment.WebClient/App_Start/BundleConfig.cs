﻿using System.Web;
using System.Web.Optimization;

namespace N_Dexed.Deployment.WebClient
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Lib/jQuery/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                        "~/Lib/jQuery/jquery-ui-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Lib/jQuery/jquery.unobtrusive*",
                        "~/Lib/jQuery/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Lib/modernizr-*"));


            bundles.Add(new StyleBundle("~/Lib/KickStartStyles").Include(
                    "~/Lib/KickStart/style.css",
                    "~/Lib/KickStart/css/kickstart*",
                    "~/Lib/KickStart/css/jquery.fancybox-{version}.css",
                    "~/Lib/KickStart/css/prettify.css",
                    "~/Lib/KickStart/css/tiptip.css"
                ));

            bundles.Add(new ScriptBundle("~/Lib/KickStartScripts").Include(
                    "~/Lib/KickStart/js/kickstart.js"
                ));

            bundles.Add(new ScriptBundle("~/Lib/AngularScripts").Include(
                   "~/Lib/Angular/angular.js",
                   "~/Lib/Angular/angular-route.js"
               ));

            bundles.Add(new ScriptBundle("~/Scripts/Registration").Include(
                "~/Scripts/Registration/registration.js",
                "~/Scripts/Registration/registrationServices.js",
                "~/Scripts/Registration/registrationController.js",
                "~/Scripts/Registration/registrationConfig.js"
                ));
        }
    }
}