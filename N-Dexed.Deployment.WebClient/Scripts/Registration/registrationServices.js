﻿registration.value('baseUrl', 'http://localhost:55034/api');

registration.factory('apiServices',
                     [
                        '$http', 'baseUrl',
                        function ($http, baseUrl) {

                            return {
                                validateEmailAddress: function (emailAddress, successCallback, errorCallback) {
                                    console.log(baseUrl);
                                   
                                    var resourceUrl = baseUrl + '/Registration/' + emailAddress;
                                    $http.get(resourceUrl)
                                        .success(successCallback)
                                        .error(errorCallback)
                                },
                                
                                registerUser: function (user, successCallback, errorCallback) {
                                    var resourceUrl = baseUrl + '/Registration/';

                                    var createUserInstruction = {
                                        Id: '00000000-0000-0000-0000-000000000000',
                                        UserName: user.userName,
                                        EmailAddress: user.emailAddress,
                                        Password: user.password,
                                        CustomerName: user.companyName
                                    };

                                    var config = {
                                        instruction: createUserInstruction
                                    };

                                    console.log(config);

                                    $http.post(resourceUrl, createUserInstruction)
                                        .success(successCallback)
                                        .error(errorCallback)
                                }
                            };
                        }
                     ]);