﻿
registration.config(
                    [
                        '$routeProvider',
                        function ($routeProvider) {
                            $routeProvider.when('/', { controller: 'registrationController', templateUrl: 'registration.html' })
                        }
                    ]);