﻿
registration.controller('registrationController',
                        [
                            '$scope',
                            'apiServices',
                            function ($scope, apiServices) {
                                $scope.user = {};
                                $scope.emailInUse = false;
                                $scope.passwordsMatch = true;
                                $scope.warningMessage = '';
                                $scope.errorMessage = '';

                                $scope.initialize = function () {
                                    $scope.emailInUse = false;
                                    $scope.passwordsMatch = true;
                                };

                                $scope.submitRegistration = function () {
                                    $scope.errorMessage = '';

                                    $scope.passwordsMatch = $scope.validatePasswordMatch();
                                    if ($scope.passwordsMatch == true) {
                                        $scope.emailInUse = false;
                                        $scope.validateEmailAddress();
                                    }
                                };

                                $scope.validatePasswordMatch = function () {
                                    var passwordsMatch = false;
                                    if ($scope.user.password == $scope.user.passwordConfirm) {
                                        passwordsMatch = true;
                                    }

                                    return passwordsMatch;
                                };

                                $scope.validateEmailAddress = function () {
                                    apiServices.validateEmailAddress($scope.user.emailAddress, $scope.emailAddressValidated, $scope.emailAddressInUse);
                                };

                                $scope.emailAddressValidated = function (data, status, headers, config) {
                                    $scope.emailInUse = false;

                                    apiServices.registerUser($scope.user, $scope.userRegistered, $scope.userRegistrationError);
                                };

                                $scope.emailAddressInUse = function (data, status, headers, config) {
                                    $scope.emailInUse = true;

                                    $scope.warningMessage = $scope.cleanMessage(data);
                                };

                                $scope.userRegistered = function (data, status, headers, config) {
                                    console.log('User Registered');
                                };

                                $scope.userRegistrationError = function (data, status, headers, config) {
                                    $scope.errorMessage = $scope.cleanMessage(data);
                                };

                                $scope.resetUser = function () {
                                    $scope.user.userName = '';
                                    $scope.user.emailAddres = '';
                                    $scope.user.password = '';
                                    $scope.user.passwordConfirmation = '';
                                    $scope.user.customerName = '';
                                };

                                $scope.cleanMessage = function (message) {
                                    message = message.replace("\"", "").replace("\"", "");

                                    return message;
                                };
                            }
                        ]);